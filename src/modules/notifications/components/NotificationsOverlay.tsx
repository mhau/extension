import React, { useState, useEffect, FC } from "react";
import { fetchNotifications } from "../../../api";
import { NotificationDto } from "../../../api/types";
import styled from "styled-components";
import { Button, ChakraProvider } from "@chakra-ui/react";
import { Notification } from "./Notification";
import moment from "moment";

type NotificationsOverlayProps = {
    timestamp: Date,
    dismissNotifications: () => void
};

const Overlay = styled.div`
    position: fixed;
    left: 0;
    top: 0;
    width: 100vw;
    height: 100vh;
    background: rgba(0, 0, 0, 0.5);
    z-index: 1000;
    backdrop-filter: blur(5px);
    display: flex;
    flex-flow: column nowrap;
    justify-content: center;
    align-items: center;
`;

const Note = styled.div`
    margin-top: 1rem;
    font-size: 0.85rem;
    color: #ffffff;
    opacity: 0.75;
`;

const NotificationsOverlay: FC<NotificationsOverlayProps> = ({ timestamp, dismissNotifications }: NotificationsOverlayProps) => {
    const [notifications, setNotifications] = useState<Array<NotificationDto>>([]);
    const dismiss = () => {
        setNotifications([]);
        dismissNotifications();
    };

    useEffect(() => {
        const reference = moment(timestamp);

        fetchNotifications().then(({ notifications }) =>
            setNotifications(notifications.filter(notification => moment(notification.published).isAfter(reference)))
        );
    }, []);

    if (notifications.length === 0) {
        return <></>;
    }

    return (
        <ChakraProvider resetCSS={false}>
            <Overlay>
                {notifications.map((notification, index) => <Notification key={index} text={notification.text} link={notification.link} />)}

                <Button onClick={() => dismiss()} colorScheme="telegram" mt="4">Zavřít a již nezobrazovat</Button>
                <Note>PS. oznámení se dají kompletně vypnout v nastavení</Note>
            </Overlay>
        </ChakraProvider>
    );
};

export default NotificationsOverlay;